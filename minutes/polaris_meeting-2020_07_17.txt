Polaris Meeting - July 17, 2020


1 [2020-07-17 Fri]  - Polaris Friday
====================================

  - Attending:
    - Red
    - Xabi
    - Hugh
    - Adithya
  - Current status:
    - Adithya: Almost done exams.
  - MRs:
    - Polaris: Dask (!114): Contacted upstream to ask for help.  The MR
      itself is an optimization, not critical.
    - VV (!4): Answered most of the feedback raised.  Needs final look,
      then can merge.  Has gotten quite heavy -- can address further
      bugs later on.
      - Next: integrate into Polaris fetch. Some decisions to be made on
        how to do this:
        - Influxdb: add to Polaris?  Need to have good way of separating
          InfluxDB interactions so both can use.
      - Celestrak: Data retention times can be low for cubesats.
      - Xabi will review, merge if okay.
    - Betsi: no WIP.  Autoencoders working.  Adithya has been doing some
      work in a Jupyter notebook in the playground; some issues found
      which need further confirmation.
  - Can we do any other work in parallel?
    - Viz:
      - open issue now to discuss changes
      - add metadata to graph, display
      - more normalizers -- Adithya was working on tool to help with
        generating new normalizes
    - Jan-Peter's MR
  - Conferences
    - Smallsat
    - Red has two:
      - Startup incubator Frankfurt
      - Project Management conference
    - Let's look for more
  - Acrux change of NORAD ID -- will need to account for this when
    fetching TLE at least.
