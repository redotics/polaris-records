Hugh Brown


Table of Contents
_________________

1. [2021-03-26 Fri]  - Meeting with Patrick about polaris doc
.. 1. Attending
.. 2. Discussion: How to get feedback from satellite operators?
.. 3. Action items
..... 1. TODO Hugh: Test Polaris pipeline on Qubik
..... 2. TODO Xabi: Talk to Nikoletta and Elkos about Qubik analysis to sort out publicity
..... 3. TODO Qubik: Analysis comparing in-orbit with engineering tests
..... 4. TODO Qubik: Analysis of deployment test
..... 5. TODO Qubik: Case study
..... 6. TODO Talk to Kevin/phasewrap about BOBCAT-1


1 [2021-03-26 Fri]  - Meeting with Patrick about polaris doc
============================================================

1.1 Attending
~~~~~~~~~~~~~

  - Xabi
  - Hugh
  - Patrick


1.2 Discussion: How to get feedback from satellite operators?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  - Hugh/Xabi:
    - We'd love to start getting Polaris in front of actual satellite
      operators to get feedback.
    - We understand from Nikoletta that one of the first questions from
      operators will be "where's the documentation?"
    - While we have some, we probably need more
    - As someone who's part of the project & talks to satellite
      operators, would love to hear what you think we might need
  - Patrick
    - Anomaly detection: what's the state of that?  I'd expect to look
      into these -- we don't have visualization for this.
      - Hackathon coming up soon -- would be cool to do this with
        Grafana
    - Qubiks mission? Hard project: only a few weeks in orbit.  Two
      pocketqubes to be launched on Firefly inaugural flight.  Not ready
      yet -- end of March/April.  We have data from engineering models.
      This is an LSF project, demonstrator for SIDLOC project
      - SIDLOC will be on the quibiks; twins, deployed separately
      - LSF will operator Qubiks.  Two greek stations, one backup on
        Patrick's roof.
      - This would be a good showcase for Polaris.
      - Low orbit, matter of weeks
      - db has tests from engineering data
    - Have talked about Polaris to teams -- but they're all busy in
      early phase.  Have suggested they contact us earlier, but always
      hard if come later.
  - Are there other missions we should analyze?
    - Bobcat-1 (doh!)
    - Patrick has done a bunch of dashboards recently:
      - GRB-alpha
      - Amicalsat - having problems Alitya museal - KRAKSat. Waiting for
        millionth boot number
      - Delphi-next -- was malfunctioning for a few years, and came up a
        few weeks ago
      - Cupel-1


1.3 Action items
~~~~~~~~~~~~~~~~

1.3.1 TODO Hugh: Test Polaris pipeline on Qubik
-----------------------------------------------


1.3.2 TODO Xabi: Talk to Nikoletta and Elkos about Qubik analysis to sort out publicity
---------------------------------------------------------------------------------------


1.3.3 TODO Qubik: Analysis comparing in-orbit with engineering tests
--------------------------------------------------------------------


1.3.4 TODO Qubik: Analysis of deployment test
---------------------------------------------


1.3.5 TODO Qubik: Case study
----------------------------

  - step by step
  - lessons learned


1.3.6 TODO Talk to Kevin/phasewrap about BOBCAT-1
-------------------------------------------------
